Front-end:
Intégration d'un form html avec 4 champs input (nom, prénom, numéro de téléphone et adresse email) et un bouton d'envoi. 
Lorsqu'on clique sur le boton envoi, on envoi le form avec un appel ajax vers le back-end
Back-end:
Création d'une classe de service dans laquelle on injecte une classe abstraite IWriter. 
Créeation de 3 objets qui hérite chacun la même classe IWriter pour les injecter dans le constructeur de l'objet service.
Le premier ajoute une ligne des données du form dans un fichier text sur le disque dur.
Le 2ème ajoute un row dans une table mysql toujours avec les mêmes données du form 
et le troisième envoi un email vers l'adresse entrée dans le form et mettre comme message un tableau html du contenu du form 
branche dev1