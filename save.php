<?php
/**
 * Created by PhpStorm.
 * User: Walid
 * Date: 25/02/2018
 * Time: 15:31
 */

require 'autoload.php';

use Models\DBWriter;
use Models\FileWriter;
use Models\MailSender;
use Service\ServiceContainer;
use Models\DB;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    if (isset($_POST['data'])) {
        $data = $_POST['data'];
    }


    try {
        $formData = array();
        foreach ($data as $item) {
            $formData[$item['name']] = $item['value'];
        }
        $db = DB::getInstance();
        $dbWriter = new DBWriter($formData);
        $dbWriter->setDb($db);
        $fileWriter = new FileWriter($formData);
        $mailSender = new MailSender($formData);

        $dbServiceContainer = new ServiceContainer($dbWriter);
        $dbServiceContainer->doWork();
        $fileServiceContainer = new ServiceContainer($fileWriter);
        $fileServiceContainer->doWork();
        $mailServiceContainer = new ServiceContainer($mailSender);
        $mailServiceContainer->doWork();

        echo json_encode(array(
            'success' => array(
                'msg' => 'success',
                'code' => '200',
            ),
        ));


    } catch (Exception $e) {
        echo json_encode(array(
            'error' => array(
                'msg' => $e->getMessage(),
                'code' => $e->getCode(),
            ),
        ));
    }

} else {
    echo json_encode(array(
        'error' => array(
            'msg' => 'non authorisé',
            'code' => 403,
        ),
    ));
}