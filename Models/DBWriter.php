<?php
/**
 * Created by PhpStorm.
 * User: Walid
 * Date: 25/02/2018
 * Time: 16:45
 */

namespace Models;


use Models\Resources\DBResource;

class DBWriter extends IWriter
{
    private $db;

    public function setDb($db)
    {
        $this->db = $db;
    }

    public function doWork()
    {
        $dbResource = new DBResource($this->db);
        $dbResource->save($this->name, $this->surname, $this->email, $this->tel);
    }
}