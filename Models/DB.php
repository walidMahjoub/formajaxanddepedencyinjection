<?php
/**
 * Created by PhpStorm.
 * User: Walid
 * Date: 25/02/2018
 * Time: 16:48
 */

namespace Models;


class DB extends \PDO
{
    const DB_HOST = 'localhost';
    const DB_NAME = 'test';
    const DB_USERNAME = 'root';
    const DB_PASSWORD = '';

    private static $db;


    public static function getInstance()
    {
        if(!self::$db){
            self::$db = new self('mysql:host=' . self::DB_HOST . ';dbname=' . self::DB_NAME . ';charset=utf8', self::DB_USERNAME, self::DB_PASSWORD);
        }
        return self::$db;
    }
}