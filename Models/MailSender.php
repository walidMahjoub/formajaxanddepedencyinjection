<?php
/**
 * Created by PhpStorm.
 * User: Walid
 * Date: 25/02/2018
 * Time: 16:23
 */

namespace Models;


class MailSender extends IWriter
{
    public function doWork()
    {
        mail($this->email, 'sujet', $this->getMailBody());
    }

    private function getMailBody()
    {
        return $this->name . $this->surname . $this->tel ;
    }

}