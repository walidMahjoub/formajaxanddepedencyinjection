<?php
/**
 * Created by PhpStorm.
 * User: Walid
 * Date: 25/02/2018
 * Time: 16:46
 */

namespace Models\Resources;


use Models\IWriter;

class DBResource
{
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function save($name, $surname, $email, $tel)
    {
        $req = $this->db->prepare('INSERT INTO tast_table(nom, prenom, email, tel) VALUES(:nom, :prenom, :email, :tel)');
        $req->execute(array(
            'nom' => $name,
            'prenom' => $surname,
            'email' => $email,
            'tel' => $tel
        ));
    }

}