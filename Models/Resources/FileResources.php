<?php
/**
 * Created by PhpStorm.
 * User: Walid
 * Date: 25/02/2018
 * Time: 16:34
 */

namespace Models\Resources;


class FileResources
{
    const FILE_NAME = 'out.txt';
    const FILE_PATH = '../../out/';

    public function writeLineInFile(string $formData)
    {
        $myfile = fopen(dirname(__FILE__) . '/' . self::FILE_PATH . self::FILE_NAME, "w");
        fwrite($myfile, $formData);
        fclose($myfile);
    }

}