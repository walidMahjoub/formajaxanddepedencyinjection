<?php
/**
 * Created by PhpStorm.
 * User: Walid
 * Date: 25/02/2018
 * Time: 16:30
 */

namespace Models;

use Models\Resources\FileResources;


class FileWriter extends IWriter
{
    public function doWork()
    {
        $message = $this->email . '|' . $this->surname . '|' . $this->name . '|' . $this->tel;
        $fileResources = new FileResources();
        $fileResources->writeLineInFile($message);
    }
}