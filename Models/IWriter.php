<?php
/**
 * Created by PhpStorm.
 * User: Walid
 * Date: 25/02/2018
 * Time: 16:15
 */

namespace Models;


abstract class IWriter
{
    protected $name;
    protected $surname;
    protected $email;
    protected $tel;


    public function __construct(array $data)
    {
        $this->name = $data['name'];
        $this->surname = $data['surname'];
        $this->email = $data['email'];
        $this->tel = $data['tel'];
    }

    public abstract function doWork();

}