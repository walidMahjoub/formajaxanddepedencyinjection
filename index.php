<!DOCTYPE html>
<html lang="en">
<head>
    <title>Exercice</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="http://malsup.github.io/jquery.blockUI.js"></script>
</head>
<body>

<div class="container">
    <h2>Exercice</h2>
    <div class="alert alert-success" style="display: none">
        <strong>Success!</strong> Message de success.
    </div>
    <div class="alert alert-danger" style="display: none">
        <strong>Erreur!</strong> Message d'erreur.
    </div>
    <form class="form-inline" method="post" onsubmit="sendRequest(event)">
        <div class="form-group">
            <label for="name">Prenom:</label>
            <input type="text" class="form-control" id="name" placeholder="Prenom" name="name" required>
        </div>
        <div class="form-group">
            <label for="surname">Nom:</label>
            <input type="text" class="form-control" id="surname" placeholder="Nom" name="surname" required>
        </div>
        <div class="form-group">
            <label for="surname">Email:</label>
            <input type="email" class="form-control" id="email" placeholder="Email" name="email" required>
        </div>
        <div class="form-group">
            <label for="surname">Tel:</label>
            <input type="text" class="form-control" id="tel" placeholder="Tel" name="tel" required minlength="8">
        </div>
        <button type="submit" class="btn btn-default" id="btSubmitForm">Envoyer</button>
    </form>
</div>
<script>
    // unblock when ajax activity stops
    function sendRequest(e) {
        e.preventDefault();
        var formData = $("form").serializeArray();
        $("#btSubmitForm").attr("disabled", "disabled");
        $.blockUI();
        $.ajax({
            url: "/exercice/save.php ",
            method: "POST", //First change type to method here

            data: {
                data: formData
            },
            success: function (response) {
                $('.alert-success').css("display","block");
            },
            error: function () {
                $('.alert-danger').css("display","block");
            },

            complete: function () {
                setTimeout($.unblockUI, 2000);
            }


        });

    }
</script>
</body>
</html>


