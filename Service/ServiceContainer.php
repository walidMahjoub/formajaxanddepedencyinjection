<?php
/**
 * Created by PhpStorm.
 * User: Walid
 * Date: 25/02/2018
 * Time: 17:04
 */

namespace Service;


use Models\IWriter;

class ServiceContainer
{
    private $writer;

    public function __construct(IWriter $writer)
    {
        $this->writer = $writer;
    }

    public function doWork()
    {
        $this->writer->doWork();
    }
}